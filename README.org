#+TITLE: CL-Devel Docker Images

This project contains Dockerfiles to build images geared toward arbitrary
Common Lisp development and the infrastructure to build the images.

This repository is mirrored between both
[[https://gitlab.common-lisp.net/cl-docker-images/cl-devel]] and
[[https://github.com/cl-docker-images/cl-devel]].

* Unofficial images

  Currently, all images defined on the =master= branch are built and pushed to
  =clfoundation/cl-devel= on Docker Hub.
