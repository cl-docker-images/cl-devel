ARG abcl_version=1.8.0
ARG ccl_version=1.12
ARG clasp_version=0.4.3
ARG clisp_version=2.49.92
ARG ecl_version=21.2.1
ARG sbcl_version=2.2.1

ARG fixuid_version=0.5
ARG asdf_version=3.3.5
ARG cl_launch_version=4.1.3

ARG user=cl
ARG group=cl
ARG uid=1000
ARG gid=1000

ARG quicklisp_client_version=2021-02-13
ARG quicklisp_dist_version=2021-12-30

FROM clfoundation/abcl:$abcl_version-jdk11 as abcl
FROM clfoundation/ccl:$ccl_version as ccl
FROM clfoundation/clasp:$clasp_version as clasp
FROM clfoundation/clisp:$clisp_version as clisp
FROM clfoundation/ecl:$ecl_version as ecl
FROM clfoundation/sbcl:$sbcl_version-buster as sbcl

FROM buildpack-deps:buster

# Install all the packages needed for Quicklisp systems
RUN set -x \
    && package_list="openjdk-11-jdk-headless gosu \
    swig libopenmpi-dev libusb-dev libpython2.7-dev cvs screen build-essential \
    libsdl1.2debian libsdl-image1.2 libsdl-gfx1.2-dev libsdl-mixer1.2-dev libuv1-dev primus-libs \
    libpython2.7 libgtkglext1-dev libgtk2.0-dev libglu1-mesa freeglut3-dev \
    libdrm-dev libgbm-dev libegl1-mesa-dev libsdl2-2.0-0 libhdf5-dev libblas-dev liblapack-dev \
    libfixposix-dev libgtk2.0-0 libgtk-3-0 \
    libdevil-dev libenchant-dev libev-dev libfam-dev libfcgi-dev libfbclient2 libfreeimage-dev \
    libfuse-dev libgeoip-dev libgeos-dev libgit2-dev libftgl2 libglu1-mesa-dev libgl1-mesa-dev \
    libglfw3-dev libglfw3 libgraphviz-dev libkyotocabinet-dev liballegro-acodec5-dev \
    liballegro-audio5-dev liballegro-dialog5-dev liballegro-image5-dev liballegro-physfs5-dev liballegro5-dev \
    liballegro-ttf5-dev libpuzzle1 liblinear-dev libsvm3 \
    libblas3 liblapack3 freetds-dev ocl-icd-opencl-dev libode-dev libopenal-dev libalut-dev libfann-dev \
    libglpk-dev libgsl-dev libpango1.0-dev libplplot-dev libportaudio2 libproj-dev pslib1 \
    librabbitmq-dev r-mathlib librrd-dev libsane-dev libsdl2-image-2.0-0 \
    libsdl2-ttf-2.0-0 libsoil-dev libsdl1.2-dev libtesseract-dev libtidy-dev libtokyocabinet-dev libwayland-dev \
    libsoup2.4-1 libwebkit2gtk-4.0-dev libxkbcommon-dev libassimp-dev libsdl-mixer1.2 \
    unixodbc-dev libncursesw5 libxrandr-dev libbluetooth-dev libsdl2-gfx-1.0-0 \
    libev4 libleveldb-dev libnet1-dev libsdl-ttf2.0-0 liblmdb-dev libmagic-dev libflac8 libasound2 \
    libmpg123-0 libvorbisfile3 libpcap0.8-dev gramps r-base-core libqt4-dev \
    libsnappy-dev \
    libffcall-dev libsigsegv-dev \
    libgc-dev libboost-filesystem-dev libboost-date-time-dev libboost-program-options-dev libboost-system-dev libboost-iostreams-dev llvm-6.0-dev clang-6.0 libclang-6.0-dev \
    rlwrap" \
# Differences from Quicklisp's package list (besides trivial updates and
# removing things already in buildpack-deps)
# https://github.com/quicklisp/quicklisp-controller/blob/master/debian-setup/debian-9-packages.txt
# libgfortran-8-dev replaces libgfortran3
#
# Removed: triplea libsmokeqt4-dev libsmokeqtgui4-3 emacs-nox libsqlite0-dev
#
# The second to last line is the runtime deps for CLISP
#
# The last line is the runtime deps for Clasp
    && apt-get update \
    && apt-get install -y --no-upgrade --no-install-recommends $package_list \
    && rm -rf /var/lib/apt/lists/*

# Install other packages.
RUN set -x \
    && apt-get update \
    && apt-get install -y --no-upgrade --no-install-recommends sudo \
    && rm -rf /var/lib/apt/lists/*

ARG user
ARG group
ARG uid
ARG gid

# Add the Quicklisp installer.
WORKDIR /usr/local/share/common-lisp/source/quicklisp/

ENV QUICKLISP_SIGNING_KEY=D7A3489DDEFE32B7D0E7CC61307965AB028B5FF7

RUN set -x \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp" > quicklisp.lisp \
    && curl -fsSL "https://beta.quicklisp.org/quicklisp.lisp.asc" > quicklisp.lisp.asc \
    && GNUPGHOME="$(mktemp -d)" \
    && export GNUPGHOME \
    && gpg --batch --keyserver ha.pool.sks-keyservers.net --recv-keys "${QUICKLISP_SIGNING_KEY}" \
    && gpg --batch --verify "quicklisp.lisp.asc" "quicklisp.lisp" \
    && rm quicklisp.lisp.asc \
    && rm -rf "$GNUPGHOME"

# Create a non-privileged user to normally operate as.
RUN set -x \
    && addgroup --gid $gid $group \
    && adduser --uid $uid --ingroup $group --home /home/$user --shell /bin/sh --disabled-password --gecos "" $user \
    && adduser $user sudo \
    && chown -R $user:$group /usr/local/ \
    && echo "$user ALL=NOPASSWD: ALL" >> /etc/sudoers.d/$user

# Install fixuid
ARG fixuid_version
RUN set -x \
    && curl -SsL https://github.com/boxboat/fixuid/releases/download/v$fixuid_version/fixuid-$fixuid_version-linux-amd64.tar.gz | tar -C /usr/local/bin -xzf - \
    && chown root:root /usr/local/bin/fixuid \
    && chmod 4755 /usr/local/bin/fixuid \
    && mkdir -p /etc/fixuid \
    && printf "user: $user\ngroup: $group\n" > /etc/fixuid/config.yml

# Install the Lisps, roughly in order of "least frequent updates" to "most
# frequent updates"

# CLISP determines the default encoding from LANG. Set to something likely to
# be sane.
ENV LANG C.UTF-8

ARG clisp_version
ENV CLISP_VERSION $clisp_version
COPY --from=clisp /usr/local/src/clisp /usr/local/src/clisp
COPY --from=clisp /usr/local/bin/clisp /usr/local/bin/clisp
COPY --from=clisp /usr/local/bin/clisp-link /usr/local/bin/clisp-link
COPY --from=clisp /usr/local/lib/clisp-$CLISP_VERSION /usr/local/lib/clisp-$CLISP_VERSION

ARG clasp_version
ENV CLASP_VERSION $clasp_version
COPY --from=clasp /usr/local/bin/cclasp-boehm /usr/local/bin/cclasp-boehm
COPY --from=clasp /usr/local/bin/clasp /usr/local/bin/clasp
COPY --from=clasp /usr/local/bin/fork-client /usr/local/bin/fork-client
COPY --from=clasp /usr/local/bin/iclasp-boehm /usr/local/bin/iclasp-boehm
COPY --from=clasp /usr/local/lib/clasp /usr/local/lib/clasp
COPY --from=clasp /usr/local/src/clasp /usr/local/src/clasp

ARG ecl_version
ENV ECL_VERSION $ecl_version
COPY --from=ecl /usr/local/bin/ecl /usr/local/bin/ecl
COPY --from=ecl /usr/local/bin/ecl-config /usr/local/bin/ecl-config
COPY --from=ecl /usr/local/lib/ecl-$ECL_VERSION /usr/local/lib/ecl-$ECL_VERSION
COPY --from=ecl /usr/local/lib/libecl.so.$ECL_VERSION /usr/local/lib/
COPY --from=ecl /usr/local/include/ecl /usr/local/include/ecl

RUN ln -s libecl.so.$ECL_VERSION /usr/local/lib/libecl.so.21.2 \
    && ln -s libecl.so.$ECL_VERSION /usr/local/lib/libecl.so.21 \
    && ln -s libecl.so.$ECL_VERSION /usr/local/lib/libecl.so \
    && ldconfig

ARG ccl_version
ENV CCL_VERSION $ccl_version
COPY --from=ccl /usr/local/src/ccl/ /usr/local/src/ccl/

RUN cp /usr/local/src/ccl/scripts/ccl64 /usr/local/bin/ccl

ARG abcl_version
ENV ABCL_VERSION $abcl_version
COPY --from=abcl /usr/local/share/abcl /usr/local/share/abcl
COPY --from=abcl /usr/local/bin/abcl /usr/local/bin/abcl

ARG sbcl_version
ENV SBCL_VERSION $sbcl_version
COPY --from=sbcl /usr/local/lib/sbcl /usr/local/lib/sbcl
COPY --from=sbcl /usr/local/bin/sbcl /usr/local/bin/sbcl
COPY --from=sbcl /usr/local/src/sbcl /usr/local/src/sbcl

# Install cl-launch
ARG cl_launch_version
ENV CL_LAUNCH_VERSION=$cl_launch_version

RUN set -x \
    && curl -fsSL https://gitlab.common-lisp.net/xcvb/cl-launch/-/archive/$CL_LAUNCH_VERSION/cl-launch-$CL_LAUNCH_VERSION.tar.gz | tar -xzf - \
    && cd cl-launch-$CL_LAUNCH_VERSION \
    && make install_cl

# Install ASDF. I would love to replace each implementation's provided version,
# but that process doesn't work on ABCL. Plus installing ASDF that way makes it
# ~impossible to use even a moderately older version.

USER $user:$group

ARG asdf_version
ENV ASDF_VERSION=$asdf_version
RUN set -x \
    && mkdir -p /usr/local/share/common-lisp/source/ \
    && curl -fsSL https://common-lisp.net/project/asdf/archives/asdf-$ASDF_VERSION.tar.gz | tar -C /usr/local/share/common-lisp/source -xzf - \
    && mv /usr/local/share/common-lisp/source/asdf-$ASDF_VERSION /usr/local/share/common-lisp/source/asdf

# Install Quicklisp
WORKDIR /home/cl

ARG quicklisp_client_version
ARG quicklisp_dist_version

ENV QUICKLISP_CLIENT_VERSION=$quicklisp_client_version
ENV QUICKLISP_DIST_VERSION=$quicklisp_dist_version
ENV QUICKLISP_LOAD=yes

RUN set -x \
    && sbcl --non-interactive --load /usr/local/share/common-lisp/source/quicklisp/quicklisp.lisp --eval "(quicklisp-quickstart:install :client-version \"$QUICKLISP_CLIENT_VERSION\" :dist-version \"quicklisp/$QUICKLISP_DIST_VERSION\")" \
    && rm -rf .cache/common-lisp

COPY --chown=$user:$group lisprc .sbclrc
COPY --chown=$user:$group lisprc .ccl-init.lisp
COPY --chown=$user:$group lisprc .eclrc
COPY --chown=$user:$group lisprc .abclrc

# Create a copy of the default home directory so users can unpack it into a
# directory they mount over /home/cl
WORKDIR /home

RUN set -x \
    && tar c cl > /usr/local/share/common-lisp/cl-home.tar

COPY --chown=$user:$group unpack-default-home-dir /usr/local/bin/unpack-default-home-dir

WORKDIR /

ENTRYPOINT ["fixuid"]
CMD ["bash"]
